#!/usr/bin/env python3

import argparse
from datetime import date
import os.path
import re
import subprocess as sub
import sys
import tempfile
import time
from urllib.parse import urlparse, urlunparse

from bs4 import BeautifulSoup  # https://pypi.org/project/beautifulsoup4/
import requests  # https://pypi.org/project/requests/
#####


###  Main  ###
def main(args, env):
    (board_path, dl_dir) = default_dirs(env)  # TODO: add parser epilog describing env vars
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-b", "--board-path",
        action="store",
        default=board_path,
        metavar="PATH",
        help=f"The path to the 'drive' presented by the board, defaults to {board_path}",
    )
    parser.add_argument(
        "-d", "--downloads-dir",
        action="store",
        default=dl_dir,
        metavar="PATH",
        help=f"The path to download assets to, defaults to {dl_dir}",
    )
    parser.add_argument(
        "-f", "--force",
        action="store_true",
        help="Update the board whether it needs it or not.",
    )
    parser.add_argument(
        "-l", "--lib-version",
        action="store",
        type=int,
        metavar="M",
        help="Use major version M of the library bundle.",
    )
    parser.add_argument(
        "-s", "--slug",
        action="store",
        metavar="SLUG",
        help="Specify a slug to find the board's page like this: https://circuitpython.org/board/SLUG/.",
    )
    parser.add_argument(
        "-u", "--unstable",
        dest="release",
        action="store_const",
        const="unstable",
        default="stable",
        help="Install the unstable release.",
    )
    parser.add_argument(
        "--dry-run",
        action="store_true",
        help="Don't update, just gather info and downloads.",
    )
    parser.add_argument(
        "--print-args",
        action="store_true",
        help="Print arguments, then exit.",
    )
    my_args = parser.parse_args(args[1:])

    if my_args.print_args:
        print(f"* Args:\n\t{my_args}")
        return 0

    if not os.path.exists(my_args.board_path):
        return f"Please connect a CircuitPython board, could not find one at '{my_args.board_path}'"

    ## Gather info for update
    print("Gathering board info and links...")
    (board_ver, board_type, board_slug) = fetch_board_info(my_args.board_path)
    if my_args.slug:
        board_slug = my_args.slug
    else:
        print(f"Found board type: '{board_type}', slug: '{board_slug}'")
    board_vv = version_vector(board_ver)


    board_lib_dir = os.path.join(my_args.board_path, "lib")
    (lib_ver, lib_date, lib_list) = fetch_lib_info(board_lib_dir)
    lib_copy_all = len(lib_list) >= 100  # TODO: get the threshold from my_args or env

    uf2_link = fetch_uf2_link(board_slug, release_class=my_args.release)
    uf2_name = os.path.basename(urlparse(uf2_link).path)
    uf2_ver = uf2_version(uf2_name)
    uf2_vv = version_vector(uf2_ver)

    uf2_newer = uf2_vv > board_vv

    bundle_link = fetch_bundle_link(my_args.lib_version if my_args.lib_version else uf2_vv[0])
    bundle_name = os.path.basename(urlparse(bundle_link).path)
    (bundle_ver, bundle_date) = bundle_name_info(bundle_name)

    bundle_newer = lib_ver is None or bundle_ver > lib_ver or bundle_date > lib_date
    backup_dir = tempfile.mkdtemp(prefix="CircuitPy_")

    ## Print summary
    print(f"""CircuitPython Auto-Updater:
    Board: {board_type} Ver: {board_ver} Needs update: {uf2_newer}
    Libs: {lib_ver} ({lib_date})  Needs update: {bundle_newer}
    Lib count: {len(lib_list)}  ({"full" if lib_copy_all else "partial"} bundle)

    Latest ({my_args.release}):
    {uf2_ver}: {uf2_link}
    {bundle_ver}: {bundle_link}
    Downloads: {my_args.downloads_dir}
    Backup: {backup_dir}""")

    prompt = None
    if uf2_vv[0] > board_vv[0]:
        prompt = "Major version update"
    elif uf2_vv[3] < 0:
        prompt = "Updating to a prerelease version"
    elif board_vv[3] < 0:
        prompt = "Updating from a prerelease version"
    if prompt:
        if my_args.dry_run:
            print("[[ Dry Run ]]")
        sys.stdout.flush()
        time.sleep(.1)  # HACK: Prompt STILL sometimes prints out-of-order after flush
        yn = input(f"{prompt}, some programs may require changes; continue? (Y/n) ").lower()
        if yn in ["n", "no"]:
            sub.run(["rm", "-rf", backup_dir])
            print("Aborting")
            return 0

    if uf2_newer or my_args.force:
        downloadfile(my_args.downloads_dir, uf2_name, uf2_link, force=my_args.force)

    if bundle_newer or my_args.force:
        downloadfile(my_args.downloads_dir, bundle_name, bundle_link, force=my_args.force)
        bundle_dir = bundle_name.split(".z")[0]
        bundle_lib_dir = os.path.join(my_args.downloads_dir, bundle_dir, "lib")
        if not os.path.exists(bundle_lib_dir):
            print("Extracting libraries...")
            unzip_cmd = ["unzip", "-q", os.path.join(my_args.downloads_dir, bundle_name), "-d", my_args.downloads_dir]
            sub.run(unzip_cmd)
        if not lib_copy_all:
            bundle_list = os.listdir(bundle_lib_dir)
            (copy_libs, warn_libs, missing_libs) = lib_list_compare(lib_list, bundle_list)
            if missing_libs:
                print(f"These libraries are missing in the new bundle:")
                print("\t" + "\n\t".join(missing_libs))
            if warn_libs:
                print(f"These libraries changed substantially:")
                print("\t" + "\n\t".join(warn_libs))
            if warn_libs or missing_libs:
                sys.stdout.flush()
                time.sleep(.1)  # HACK: Prompt STILL sometimes prints out-of-order after flush
                yn = input(f"Some programs may require changes; continue? (Y/n) ").lower()
                if yn in ["n", "no"]:
                    sub.run(["rm", "-rf", backup_dir])
                    print("Aborting")
                    return 0

    if my_args.dry_run:
        sub.run(["rm", "-rf", backup_dir])
        print("Dry run done.")
        return 0

    rsync_cmd = ["rsync", "--exclude", "._*", "--exclude", "/.fseventsd", "--recursive"]
    if uf2_newer or my_args.force:
        print("Backing up board files...")
        backup_path = make_backup(my_args.board_path, backup_dir)
        dfu_vol = poll_for_dfu(my_args.board_path)
        print(f"Found DFU: {dfu_vol}")
        print(f"Copying {uf2_name}...")
        copy_files(os.path.join(my_args.downloads_dir, uf2_name), dfu_vol)
        poll_for_board(my_args.board_path)
        print("Restoring board files...")
        restore_cmd = rsync_cmd + ["--exclude", "/lib", backup_path + "/", my_args.board_path]
        sub.run(restore_cmd)
    
    if bundle_newer or my_args.force:
        print("Clearing old libraries...")
        clear_libs_cmd = ["rm", "-rf", board_lib_dir]
        sub.run(clear_libs_cmd)
        print("Copying libraries...")
        if lib_copy_all:
            copy_files(bundle_lib_dir, board_lib_dir)
        else:
            mk_libs_cmd = ["mkdir", board_lib_dir]
            sub.run(mk_libs_cmd)
            src_libs = [ os.path.join(bundle_lib_dir, l) for l in copy_libs ]
            copy_files(src_libs, board_lib_dir)
        with open(os.path.join(my_args.board_path, "lib", "lib_bundle.txt"), "wt") as l:
            l.write(bundle_name + "\n")
            l.close()

    eject(my_args.board_path)
    print("Done, the device can be removed or reset")
    sys.stdout.flush()
    time.sleep(.1)  # HACK: Prompt STILL sometimes prints out-of-order after flush
    yn = input(f"Delete backup in {backup_dir}? (Y/n) ").lower()
    if yn not in ["n", "no"]:
        sub.run(["rm", "-rf", backup_dir])

    return 0
#####


###  Fetch Info  ###
def fetch_board_info(board_path, boot_out_name="boot_out.txt"):
    boot_str = ""
    board_ver = board_type = board_slug = None
    boot_out_path = os.path.join(board_path, boot_out_name)
    if os.path.exists(boot_out_path):
        with open(boot_out_path, "rb") as b:
            boot_str = b.readline().decode("utf8").strip()
    boot_re = re.compile(r"^Adafruit CircuitPython (?P<version>[\d.]+?(-(alpha|beta)\.\d+)?) on (?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2}); (?P<maker>\S+) (?P<board>.+?) with (?P<chip>\S+)$")
    boot_m = boot_re.match(boot_str)
    if boot_m:
        board_ver = boot_m.group("version")
        board_type = boot_m.group("board")
        board_slug = make_slug(board_type)
    return (board_ver, board_type, board_slug)


def fetch_lib_info(board_lib_dir, lib_ver_name="lib_bundle.txt"):
    lib_str = ""
    lib_version_path = os.path.join(board_lib_dir, lib_ver_name)
    if os.path.exists(lib_version_path):
        with open(lib_version_path, "rb") as l:
            lib_str = l.readline().decode("utf8").strip()
    lib_list = os.listdir(board_lib_dir) if os.path.isdir(board_lib_dir) else []
    if lib_ver_name in lib_list:
        lib_list.remove(lib_ver_name)
    # FIXME: also filter out ._files
    return bundle_name_info(lib_str) + (lib_list,)


LIB_RE = re.compile(r"adafruit-circuitpython-bundle-(?P<version>[\d.x]+)-mpy-(?P<year>\d{4})(?P<month>\d{2})(?P<day>\d{2})(\.zip)?")
def bundle_name_info(bundle_name):
    bundle_ver = bundle_date = None
    lib_m = LIB_RE.match(bundle_name)
    if lib_m:
        bundle_ver = lib_m.group("version")
        bundle_date = match_date(lib_m)
    return (bundle_ver, bundle_date)


def uf2_version(uf2_name):
    uf2_ver = None
    uf2_re = re.compile(r"adafruit-circuitpython-(?P<board>\w+)-((?P<lang>\w+)-)?(?P<version>[\d.]+?(-(alpha|beta)\.\d+)?)\.uf2")
    uf2_m = uf2_re.match(uf2_name)
    if uf2_m:
        uf2_ver = uf2_m.group("version")
    return uf2_ver
#####


###  Update Helpers  ###
def make_backup(board_path, backup_dir):
    board_path = board_path.rstrip("/")
    copy_files(board_path, backup_dir)
    return os.path.join(backup_dir, os.path.basename(board_path))


def eject(board_path):
    eject_cmd = ["diskutil", "eject", board_path]
    if sys.platform == "linux":
        eject_cmd = ["umount", board_path]
    sub.run(eject_cmd)


def poll_for_dfu(board_path):
    v_path = os.path.split(board_path.rstrip("/"))[0]
    volumes = set(os.listdir(v_path))
    eject(board_path)
    dfu_vol = None
    print("Reboot the board into update mode (double-click the reset button)")
    while not dfu_vol:
        vs = set(os.listdir(v_path))
        vs -= volumes
        vs = [ v for v in vs if v.endswith("BOOT") ]
        if vs:
            dfu_vol = vs[0]
        else:
            time.sleep(0.5)
    return os.path.join(v_path, dfu_vol)


def poll_for_board(board_path):
    print("Waiting for the device to reboot...")
    while not os.path.exists(board_path):
        time.sleep(0.5)
    time.sleep(2)  # HACK: give the board some time to be ready for activity


def lib_list_compare(lib_list, bundle_list):
    copy_libs = []
    warn_libs = []
    missing_libs = []
    for l in lib_list:
        if l in bundle_list:
            # Lib found, all good
            copy_libs.append(l)
            continue
        base = l.split(".mp")[0]
        if base in bundle_list:
            # .mpy lib changed to directory lib
            copy_libs.append(base)
            warn_libs.append(base)
            continue
        pos_new_file_name = l + ".mpy"
        if pos_new_file_name in bundle_list:
            # directory lib changed to .mpy lib
            copy_libs.append(pos_new_file_name)
            warn_libs.append(pos_new_file_name)
            continue
        # Lib is missing
        missing_libs.append(l)
    return (copy_libs, warn_libs, missing_libs)
#####


###  Utilities  ###
def default_dirs(env):
    """Check for default locations for media, downloads, etc. based on system, environment, etc"""
    if sys.platform == "darwin":
        bp = "/Volumes/CIRCUITPY"
        dl = os.path.expandvars("$HOME/Downloads")
    elif sys.platform == "linux":
        bp = os.path.expandvars("/media/$USER/CIRCUITPY")
        dl = os.path.expandvars("$HOME/Downloads")

    if "CPYUP_BOARD_PATH" in env:
        bp = env["CPYUP_BOARD_PATH"]
    if "CPYUP_DOWNLOADS" in env:
        dl = env["CPYUP_DOWNLOADS"]

    return (bp, dl)


def copy_files(src, dst):
    copy_cmd = ["cp", "-R",]
    if sys.platform == "darwin":
        copy_cmd += ["-X",]  # macOS creates extra '._' files unless we tell it not to
    copy_cmd.extend([src,] if type(src) == str else src)
    copy_cmd.append(dst)
    sub.run(copy_cmd)


SLUG_MAP = {  # Overrides for boards with non-standard slugs
    # "CircuitPlayground Express": "circuitplayground_express",
}
def make_slug(s):
    return SLUG_MAP.get(s, s.lower().replace(" ", "_"))


def version_vector(ver, num_fields=5):
    """Converts a version string into a vector that can be directly compared
    By default it assumes 5 fields: 3 version fields, and possible alpha/beta with one pre-release version; e.g. 5.0.0-alpha.1"""
    if not ver:
        ver = "0"
    ver = ver.replace("-alpha", ".-2").replace("-beta", ".-1")
    vec = [ int(i) for i in ver.split(".") ] + [0,] * num_fields
    return vec[:num_fields]


def match_date(m):
    d = m.groupdict()
    return date(int(d.get("year", None)), int(d.get("month", 1)), int(d.get("day", 1)))


UF2_HOME = "https://circuitpython.org/board/{slug}/"
def fetch_uf2_link(slug, release_class="stable"):
    board_link = UF2_HOME.format(slug=slug)
    resp = requests.get(board_link)
    if resp.status_code == 404:
        raise RuntimeError(f"404: {board_link} not found, you may need to specify a different slug")
    if not resp.ok:
        raise RuntimeError(f"there was an error :c {resp.status_code}\n{resp.text}")

    htmldata = resp.text
    soup = BeautifulSoup(htmldata, "html.parser")
    links = soup.find_all('a', class_=f"download-button {release_class} uf2")
    if not links:
        raise RuntimeError(f"could not find {release_class} download link on {board_link}")

    download_link = links[0].get('href')
    return download_link


BUNDLE_HOME = "https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/latest"
def fetch_bundle_link(major_version):
    resp = requests.get(BUNDLE_HOME)
    if resp.status_code == 404:
        raise RuntimeError(f"404: {BUNDLE_HOME} not found")
    if not resp.ok:
        raise RuntimeError(f"there was an error :c {resp.status_code}\n{resp.text}")

    htmldata = resp.text
    soup = BeautifulSoup(htmldata, "html.parser")
    links = soup.find_all(href=re.compile(f"adafruit-circuitpython-bundle-{major_version}\\.x-mpy-\\d+\\.zip"))
    if not links:
        raise RuntimeError(f"could not find {major_version}.x download link on {BUNDLE_HOME}")

    download_link = links[0].get('href')
    p = urlparse(BUNDLE_HOME)  # HACK: make link a full absolute url
    return urlunparse((p.scheme, p.netloc, download_link, p.params, p.query, p.fragment))


def downloadfile(downloadsdir, filename, url, force=False):
    """Download the file at the specified URL. Syntax: downloadfile(downloadsdir, filename, url)"""
    dst = os.path.join(downloadsdir, filename)
    if os.path.exists(dst) and not force:
        print(f"Using existing {dst}")
        return

    with open(dst, 'wb') as f:
        r = requests.get(url)
        f.write(r.content)
        f.close()
#####


#####
if __name__ == "__main__":
    import os
    xit = main(sys.argv, os.environ)
    sys.exit(xit)
#####
