# CircuitPython Updater

Update your CircuitPython-Compatible Boards In A Snap!

CircutPython Updater is an open source Python project to semi-automatically keep your CircutPython boards up to date.

## Goals and Features

- Update CircuitPython and corresponding library bundle
- Default case should be automatic, with reasonable additional flexibility
- Back-up board files before updating and restoring
- Attempt to warn of compatibility concerns related to major versions, pre-release versions, or library re-organization
- Install stable or unstable (pre-release) versions
- Update full library bundle _**or**_ just the ones installed
- Runs on macOS and Rasbian (and maybe others?)

## Requirements

- Python 3.6+
- BeautifulSoup4: `pip install beautifulsoup4`
- Requests: `pip install requests`
- Device type must be registered on [circuitpython.org]

## Running

After connecting your CircuitPython board, the updater can be run directly on the command-line like this:

`$ ./src/cpyupdate.py`

Run with `-h` to see all the options.  There are also scripts to set up desktop shortcuts you can use and more information [on the wiki](https://gitlab.com/CodeyDev/circuit-python-updater/wikis/Installation,-Setup,-and-Usage)

## How does it work?

The updater starts by checking for the "board path" directory, and tries to extract the board type (e.g. "Trinket M0") and installed version from `boot_out.txt`; it also looks for `lib/lib_bundle.txt` where it stores the version of the library bundle it installs.

It then attempts to guess the board's URL on [circuitpython.org] based on a "slug" from the board name and extracts the download link for the board's build of CircuitPython, and gets the download link for the latest corresponding library bundle.

After comparing the version numbers, a summary of what needs updating is printed; if the major version is changing or either of the versions was a pre-release build, it also prompts before continuing, since there may be incompatible changes.

Finally, the UF2 and/or library bundle are downloaded if necessary, the board files are backed up, and the board is ejected, updated and restored.  If there were fewer than 100 items in the `lib` directory originally, only those and not the full bundle are copied onto the device.

## Known Issues

- Not set up as a PyPI package (yet)
- There may be boards that don't map name-to-url-slug correctly
    - Test your boards and [submit an issue][New Issue] to add it to the `SLUG_MAP` - be sure to include the board type found by the updater and the correct [circuitpython.org] link
    - In the meantime, you can specify the correct slug (from the last segment of the link path) manually with `-s`
- Non-standard libraries in `lib/` are _**not**_ preserved
- `cp: /Volumes/CIRCUITPY/.Trashes: Operation not permitted` is a harmless error that sometimes appears during backup
- As normally happens when updating a CircuitPython UF2, the device automatically reboots and a warning appears that the `*BOOT` drive "was not ejected properly"; this is also harmless
- Also [check the open issues on GitLab](https://gitlab.com/CodeyDev/circuit-python-updater/issues?scope=all&utf8=✓&state=opened)

## **Unknown** Issues

[Send us the details!][New Issue]  Please include the full command being run, its full output, which host OS, the board type, the link to the board's page on [circuitpython.org], and anything else that might help us understand what went wrong

[New Issue]: https://gitlab.com/CodeyDev/circuit-python-updater/issues/new
[circuitpython.org]: https://circuitpython.org/downloads
