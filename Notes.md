CircuitPython Board Auto-Updater
================================

To Do
-----

- `pip` packaging
- support languages other than English

### Doneyard

- DONE: how to tell what board is plugged in?
    - PUNT: can we get that from `board` module somehow?
    - PUNT: small text file might be simplest; no need to serial into the board, can store last-checked-time, etc...
    - DONE: by default some info in `boot_out.txt`
        > Adafruit CircuitPython 4.1.0 on 2019-08-02; Adafruit CircuitPlayground Express with samd21g18
    - DONE: how do we tell what lib bundle is there?
        - DONE: write bundle name to a text file?  if bundle.txt isn't there, update anyway and add it

- DONE: map board to website / download link
- DONE: download CircuitPython and library bundle
- DONE: cache downloads

- DONE: make url from slug derrived from board name
    - DONE: allow specifying board slug
    - DONE: mapping board name --> slug
    - PUNT: allow use of explicit urls, maybe different naming than the adafruit system
    - PUNT: and/or local files
    - PUNT: or config file with local cache paths etc.

- DONE: update:
    - DONE: backup files
    - DONE: prompt for reset
    - DONE: copy .uf2
    - DONE: restore files
    - DONE: copy over libraries (all or just the ones that were there)

- DONE: `-f/--force` update
    - DONE: allow version parsing etc. to fail if `-f`

- DONE: linux support (esp. Rasbian)

---
